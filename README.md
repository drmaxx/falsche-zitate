# falsche Zitate

Fast jeder kennt inzwischen die falsch zugeordneten Zitate vom Känguru und Mark Uwe Kling.

Ich sammle hier für einen `fortune` file zur privaten Belustigung und evtl. Nutzung in diversen Nebenprojekten. 

Natürlich ist das daher weder vollständig, noch möchte ich irgendwem auf die Füße treten.  
Bitte melden, wenn sich irgendwer in seinen Rechten beeinträchtigt fühlt.

Ansonten gilt:  
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# Quellen
Die Grundlage bildet die Sammlung(en) von
  * [Ludger Sandig](http://lsandig.org/blog/2015/06/falsch-zugeordnete-zitate-fortune-edition/)
  

## How-To
Create custom fortune cookies files

  - Open your favourite editor and add the strings that you want to be shown when running `fortune` in terminal. BE SURE to add a single line with a letter `%` in it, between every string.
  - Save this file to whatever file name you want, this guide uses `yourlist` as example.
  - When done adding strings, run the command `strfile -c % yourlist yourlist.dat`. This will create a `.dat` file for your cookie file, which contains a header structure and a table of file offsets for each group of lines. This allows random access of the strings.
  - Run `fortune yourlist` to eat the fruit of your work. That's it! 
  - If you want this cookie file to appear as any other of the standard fortune cookies (when simply running `fortune`), just add your string file and `.dat` file into `/usr/share/games/fortunes/`.
  - Oh, and one more thing: if you want to change anything in `yourfile`, you will need to repeat this HOWTO again for it to work.

(Copied from the [Ubuntu Forum](https://ubuntuforums.org/showthread.php?t=1343692))
